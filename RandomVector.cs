using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Schema;

namespace file
{
    internal class RandomVector
    {
        double[] data;
        public RandomVector(int length, int min = -100, int max = 100)
        {
            data = randomVector(length, min, max);
        }
        public RandomVector(DataGridView grid)
        {
            data = readDataFromGrid(grid);
        }
        public static double[] randomVector(int length, int min = -100, int max = 100)
        {
            double[] vect = new double[length];
            Random rand = new Random(DateTime.Now.Millisecond);
            for (int j = 0; j < length; j++)
            {
                vect[j] = rand.Next(min, max);
            }
            return vect;
        }
        public static double[] readDataFromGrid(DataGridView grid)
        {
            double[] vect = new double[grid.ColumnCount];
            for (int j = 0; j < grid.ColumnCount; j++)
            {
                vect[j] = Convert.ToDouble(grid[j, 0].Value);

            }
            return vect;
        }
        public static void writeToGrid(double [] data,DataGridView grid)
        {
            grid.ColumnCount = data.Length;
            for (int j = 0; j < data.Length; j++)
            {
                grid[j, 0].Value = data[j];
            }
        }
        public static void fiilWithRandomValue(DataGridView grid, double min=-100,double max = 100)
        {
            double[] vect = RandomVector.randomVector(grid.ColumnCount);
            RandomVector.writeToGrid(vect, grid);
        }
       public static void saveVectToFile(string fileName, double[] vect)
        {
            StreamWriter sw = new StreamWriter(fileName);
            
            sw.WriteLine(vect.GetLength(1));
            for (int j = 0; j < vect.Length; j++)
            {
                sw.WriteLine(vect[j]);
            }
            sw.Close();
        }
        ///////////
        public static void saveVectToBinaryFile(string fileName, double[] vect)
        {
            FileStream FS = new FileStream(fileName, FileMode.Create);
            BinaryWriter BW = new BinaryWriter(FS);
            BW.Write(vect.GetLength(1));
            for (int j = 0; j < vect.Length; j++)
            {
                BW.Write(vect[j]);
            }
            BW.Close();
            FS.Close();
        }
        //////////////////
        public static double[] readVectFromFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            int columnsCount = br.Read();
            double[] vect = new double[columnsCount];
            for(int j=0; j < columnsCount; j++)
            {
                vect[j] = Convert.ToDouble(br.Read());

            }
            br.Close();
            fs.Close();
            return vect;
        }
        /////////////////////////
        public static double[] readVectFromBinaryFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);

            int columnsCount = Convert.ToInt32(sr.ReadLine());
            double[] vect = new double[columnsCount];
            for (int j = 0; j < columnsCount; j++)
            {
                vect[j] = Convert.ToDouble(sr.ReadLine());

            }
            sr.Close();
            return vect;
        }
        ////////////////////////////
        public static void saveGridToFile(string fileName,DataGridView grid)
        {
            double[] vect = readDataFromGrid(grid);
            saveVectToFile(fileName, vect);
        }
        ////////////////////////////////////////
        public static void saveGridToBinaryFile(string fileName, DataGridView grid)
        {
            double[] vect = readDataFromGrid(grid);
            saveVectToBinaryFile(fileName, vect);
        }
        /////////////////////////////////
        public static void readGridDataFromFile(string fileName, DataGridView grid)
        {
            double[] vect = readVectFromFile(fileName);
            writeToGrid(vect, grid);
        }
        /////////////////////////////////////////////////////////////
        public static void readGridDataFromBinaryFile(string fileName, DataGridView grid)
        {
            double[] vect = readVectFromBinaryFile(fileName);
            writeToGrid(vect, grid);
        }
    }
}
