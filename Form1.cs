using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace file
{
    public partial class Form1 : Form
    {
      
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 3;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }

        private void згенеруватиМасивToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RandomVector.fiilWithRandomValue(dataGridView1);
        }

        private void вийтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void знайтиНайменшеЗначенняСередДодатніхToolStripMenuItem_Click(object sender, EventArgs e)
        {
           double[] vect = RandomVector.readDataFromGrid(dataGridView1);
            double m = 1000;
            foreach(var item in vect)
            {
                if (item > 0 && item < m)
                {
                    m = item;
                }
                
                  
            }
            MessageBox.Show(m.ToString());
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RandomVector.saveGridToFile(saveFileDialog1.FileName, dataGridView1);

            }
        }

        private void завантажитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RandomVector.readGridDataFromFile(openFileDialog1.FileName, dataGridView1);
                numericUpDown1.Value = dataGridView1.RowCount;
                numericUpDown1.Value = dataGridView1.ColumnCount;
            }
        }

        private void зберегтиБінарнийФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RandomVector.saveGridToBinaryFile(saveFileDialog1.FileName, dataGridView1);

            }

        }

        private void завантижитиБінарнийФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RandomVector.readGridDataFromBinaryFile(openFileDialog1.FileName, dataGridView1);
                numericUpDown1.Value = dataGridView1.RowCount;
                numericUpDown1.Value = dataGridView1.ColumnCount;
            }
        }
    }
}
